﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	public Slider volumeSlider;

	void Start() {
		if(PlayerPrefs.HasKey("OptionVolume")) {
			this.volumeSlider.value = PlayerPrefs.GetFloat("OptionVolume");
		} else {	
			this.volumeSlider.value = 1;
		}
		
	}

	public void PlayGame () {
		SceneManager.LoadScene(1);
	}

	public void ExitGame () {
		Application.Quit();
	}

	public void UpdateVolume() {
		PlayerPrefs.SetFloat("OptionVolume", this.volumeSlider.value);
	}

	public void BackToMainMenu() {
		SceneManager.LoadScene(0);
	}

}
