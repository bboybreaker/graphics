﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsController : MonoBehaviour {

	public AudioSource backgroundMusic;
	
	void Start () {
		float volume = PlayerPrefs.GetFloat("OptionVolume");
		this.backgroundMusic.volume = volume;
	}

}
