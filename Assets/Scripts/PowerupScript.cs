﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupScript : MonoBehaviour {

	HUDScript hud;
	public AudioSource up;

	void OnTriggerEnter(Collider other) {
		hud = GameObject.Find("Main Camera").GetComponent<HUDScript>();
		if(this.tag == "Gem" && other.tag == "Player") {
			AudioSource.PlayClipAtPoint(this.up.clip, hud.transform.position);
			this.up.Play();
			hud.IncreaseScore(1);
		}
		Destroy(this.gameObject);
	}
}
