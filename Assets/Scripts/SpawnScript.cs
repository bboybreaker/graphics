﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour {
	public bool ready = false;
	public GameObject[] obj;
	public float spawnTime = 2.7f;

	void Start () {
		Spawn();
	}

	void Spawn() {
		if(!ready) {
			Invoke("Spawn", this.spawnTime);
			return;
		}
		GameObject spObject = obj[Random.Range(0, obj.Length)];
		if(spObject.tag == "Gem" || spObject.tag == "Bomb" || spObject.tag == "Enemy") {
			Vector3 pos = transform.position;
			pos.x = Random.Range(4, -4);
			if(spObject.tag == "Enemy") {
				spObject.transform.rotation = Quaternion.Euler(0, 180, 0);
				Instantiate(spObject, pos , spObject.transform.rotation);
			} else {
				Instantiate(spObject, pos , Quaternion.identity);
			}
		} else {
			Instantiate(spObject, transform.position , Quaternion.identity);
		}
		Invoke("Spawn", this.spawnTime);
	}

}

