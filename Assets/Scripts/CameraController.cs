﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public GameObject player;
	private Vector3 vector3;

	// Use this for initialization
	void Start () {
		this.vector3 = transform.position - this.player.transform.position;
	}

	void LateUpdate () {
		var otherPosn = new Vector3(0 ,0 , this.player.transform.position.z);
		transform.position = otherPosn + this.vector3;
	}
}
