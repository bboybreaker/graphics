﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BombScript : MonoBehaviour {

	public AudioSource up;

	void OnTriggerEnter(Collider other) {
		if(this.tag == "Bomb" && other.tag == "Player") {
			AudioSource.PlayClipAtPoint(this.up.clip, transform.position);
			this.up.Play();
			SimpleCharacterControl PlayerScript = GameObject.Find("Player").GetComponent<SimpleCharacterControl>();
			PlayerScript.ready = false;
			Invoke("GameOver", 3);
		}
	}
	
	void GameOver() {
		SceneManager.LoadScene(2);
	}
}
