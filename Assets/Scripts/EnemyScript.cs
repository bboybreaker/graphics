﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyScript : MonoBehaviour {

	public float distinationToAttack = 15f;
	public float followSpeed = 10f;
	public GameObject target;
	Animation_Test animationScript;
	bool attacking = false;
	bool followed = false;
	
	// Update is called once per frame
	void Update () {
		target = GameObject.Find("Player");
		animationScript = GetComponent<Animation_Test>();
		float dist = Vector3.Distance(target.transform.position, transform.position);
		if(dist < distinationToAttack) {
			if(dist < distinationToAttack / 2) {
				if(!attacking) {
					animationScript.AttackAni();
					attacking = true;
					followed = true;
				}
			} else if(!followed) {
				attacking = false;
				animationScript.RunAni();
				float step = followSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
			}
		} else {
			animationScript.IdleAni();
		}
	}

	void OnTriggerEnter(Collider other) {
		if(other.tag == "Player" && this.tag == "Enemy") {
			animationScript.DeathAni();
			SimpleCharacterControl PlayerScript = target.GetComponent<SimpleCharacterControl>();
			PlayerScript.ready = false;
			Invoke("GameOver", 2);
		}
	}

	void GameOver() {
		SceneManager.LoadScene(2);
	}
}

