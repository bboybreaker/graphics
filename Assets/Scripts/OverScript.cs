﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OverScript : MonoBehaviour {

	int score = 0;

	public Text ScoreText;

	// Use this for initialization
	void Start () {
		ScoreText.text = "Score: " + PlayerPrefs.GetInt("Score");
	}
	
}
