﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StartTimerScript : MonoBehaviour {

	public int counts = 4;
	public TextMeshProUGUI text;
	public GameObject[] Spawners;

	void Start() {
		
		CountDown();
	}

	void CountDown() {
		if(counts != 1) {
			counts--;
		} else {
			text.text = "GO!";
			Invoke("EnableControl", 2);
			return;
		}
		text.text = counts.ToString();
		Invoke("CountDown", 1);
	}

	void EnableControl() {
		Destroy(text.gameObject.transform.parent.gameObject);
		SimpleCharacterControl PlayerScript = GameObject.Find("Player").GetComponent<SimpleCharacterControl>();
		PlayerScript.ready = true;
		AudioSource music = GameObject.Find("Main Camera").GetComponent<AudioSource>();
		music.Play();
		for (int i = 0; i < Spawners.Length; i++)
		{
			SpawnScript spawn = Spawners[i].GetComponent<SpawnScript>();
			spawn.ready = true;
		}
		
	}

}
